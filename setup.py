# -*- coding: utf-8 -*-
# This file is part of Adiczion Wine Management Tryton Module.  The 
# COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.

from setuptools import setup
import re
import os
import ConfigParser


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

config = ConfigParser.ConfigParser()
config.readfp(open('tryton.cfg'))
info = dict(config.items('tryton'))
for key in ('depends', 'extras_depend', 'xml'):
    if key in info:
        info[key] = info[key].strip().splitlines()
major_version, minor_version, _ = info.get('version', '0.0.1').split('.', 2)
major_version = int(major_version)
minor_version = int(minor_version)

requires = []
for dep in info.get('depends', []):
    if not re.match(r'(ir|res|webdav)(\W|$)', dep):
        requires.append('trytond_%s >= %s.%s, < %s.%s' %
                (dep, major_version, minor_version, major_version,
                    minor_version + 1))
requires.append('trytond >= %s.%s, < %s.%s' %
        (major_version, minor_version, major_version, minor_version + 1))

setup(name='trytond-sale_line_get_description',
    version=info.get('version', '0.0.1'),
    description='Sale Line Get Description',
    long_description=read('README'),
    author='Adiczion',
    url='http://www.adiczion.com',
    #download_url="http://downloads.tryton.org/" + \
    #        info.get('version', '0.0.1').rsplit('.', 1)[0] + '/',
    package_dir={'trytond.modules.sale_line_get_description': '.'},
    packages=[
        'trytond.modules.sale_line_get_description',
        #'trytond.modules.sale_line_get_description.tests',
    ],
    package_data={
        'trytond.modules.sale_line_get_description': info.get('xml', []) \
                + ['tryton.cfg', 'view/*.xml', 'locale/*.po', '*.odt'],
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Plugins',
        'Framework :: Tryton',
        'Intended Audience :: Developers',
        'Intended Audience :: Legal Industry',
        'License :: OSI Approved :: Affero GNU General Public License (AGPL)',
        'Natural Language :: Bulgarian',
        'Natural Language :: English',
        'Natural Language :: French',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Office/Business',
    ],
    license='AGPL-3',
    install_requires=requires,
    zip_safe=False,
    entry_points="""
    [trytond.modules]
    sale_line_get_description = trytond.modules.sale_line_get_description
    """,
    #test_suite='tests',
    #test_loader='trytond.test_loader:Loader',
    #tests_require=tests_require,
)
