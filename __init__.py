# -*- coding: utf-8 -*-
# This file is part Sale Line Get Description module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
from .product import *
from .sale import *


def register():
    Pool.register(
        Template,
        SaleLine,
        module='sale_line_get_description', type_='model')