# -*- coding: utf-8 -*-
# This file is part Sale Line Get Description module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

import logging
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond import backend
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool, Not
from trytond.pool import Pool, PoolMeta

__all__ = ['Template']
__metaclass__ = PoolMeta


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Template(ModelSQL, ModelView):
    __name__ = "product.template"
   
    return_description = fields.Boolean('Return description',
        help='If checked add the description to the name of the product when '
            'it use in sale.')

    @staticmethod
    def default_return_description():
        return False