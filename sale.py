# -*- coding: utf-8 -*-
# This file is part of Sale B2BC Tryton Module.  The 
# COPYRIGHT file at the top level of this repository contains the
# full copyright notices and license terms.

import logging
from decimal import Decimal
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

__all__ = [
    'SaleLine',
]

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

    
class SaleLine(ModelSQL, ModelView):
    __name__ = 'sale.line'

    @fields.depends('product', 'unit', 'quantity', 'description',
        '_parent_sale.party', '_parent_sale.currency',
        '_parent_sale.sale_date')
    def on_change_product(self):
        add_description = False
    
        if not self.description and self.product.template.return_description:
            add_description = True
        
        res = super(SaleLine, self).on_change_product()

        if res:
            if add_description:
                party = None
                party_context = {}
                if self.sale and self.sale.party:
                    party = self.sale.party
                    if party.lang:
                        party_context['language'] = party.lang.code
                with Transaction().set_context(party_context):
                    res['description'] = '\n'.join(filter(None, [
                        res['description'],
                        self.product.description]))
        return res